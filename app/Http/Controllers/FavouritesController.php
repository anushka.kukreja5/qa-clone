<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use Illuminate\Support\Facades\Gate;


class FavouritesController extends Controller
{
    public function store(Request $request, Question $question)
    {
        Gate::authorize('markAsFavourite',$question);
        $question->favourites()->attach(auth()->id());
        return redirect()->back();
    }

    public function destroy(Request $request, Question $question)
    {
        Gate::authorize('markAsFavourite',$question);
        $question->favourites()->detach(auth()->id());
        return redirect()->back();
    }
}
