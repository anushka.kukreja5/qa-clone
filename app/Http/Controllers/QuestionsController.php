<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Requests\Question\CreateQuestionRequest;
use App\Http\Requests\Question\UpdateQuestionRequest;

use Illuminate\Support\Facades\Gate;

use Illuminate\Routing\Controllers\HasMiddleware;
use Illuminate\Routing\Controllers\Middleware;


class QuestionsController extends Controller implements HasMiddleware
{
    /**
     * Display a listing of the resource.
     */
    public static function middleware(): array{
        return [
            new Middleware('auth',only:['create','store']),
            new Middleware('trackQuestionView',only:['show']),
        ];
    }
    public function index()
    {
        $questions = Question::with('owner')->latest()->paginate(10);
        return view('qa.questions.index',compact(['questions']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('qa.questions.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateQuestionRequest $request)
    {
        auth()->user()->questions()->create([
            'title'=> $request->title,
            'body'=> $request->body
        ]);

        session()->flash('success','Question has been added successfully!');
        return redirect(route('questions.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Question $question)
    {
        return view('qa.questions.show',compact(['question']));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Question $question)
    {
        // if(! Gate::allows('update',$question)) {
        //     abort(403);
        // }

        Gate::authorize('update',$question);
        return view('qa.questions.edit',compact(['question']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateQuestionRequest $request, Question $question)
    {
        Gate::authorize('update',$question);
        $question->update([
            'title'=> $request->title,
            'body' => $request->body
        ]);

        session()->flash('success','Question has been updated successfully!');
        return redirect(route('questions.index'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Question $question)
    {
        Gate::authorize('delete',$question);
        $question->delete();
        session()->flash('success','Question has been updated successfully!');
        return redirect(route('questions.index'));
    }
   
}
