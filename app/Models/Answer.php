<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use App\Observers\AnswerObserver;


class Answer extends Model
{   protected $guarded = [];

    use HasFactory;

    //

    #[ObservedBy([AnswerObserver::class])]

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getBestAnswerStyleAttribute()
    {
        return $this->question->best_answer_id === $this->id ? 'text-success' : '';
    }
    public function getisBestAttribute()
    {
        return $this->question->best_answer_id === $this->id ;

    }




}
