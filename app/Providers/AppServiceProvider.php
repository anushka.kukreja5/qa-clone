<?php

namespace App\Providers;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Gate;
use App\Models\Question;
use App\Models\User;
use App\Policies\QuestionPolicy;
use App\Models\Answer;
use App\Observers\AnswerObserver;




class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Paginator::useBootstrapFive();
        Route::bind('slug',function(string $slug){
            return Question::with('answers.author')->where('slug',$slug)->firstorFail();
        });

        Gate::define('edit-question',function(User $user, Question $question){
            return $user->id === $question->user_id;
        });
        Gate::policy(Question::class, QuestionPolicy::class);

        Answer::observe(AnswerObserver::class);

    }
}
